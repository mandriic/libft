# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mandriic <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/09/28 12:51:46 by mandriic          #+#    #+#              #
#    Updated: 2021/09/28 12:51:51 by mandriic         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror
NAME = libft.a
SRC = $(shell find . -name "*.c")
OBJ = $(SRC:.c=.o)
all: $(NAME)
%.o: %.c
	$(CC) $(FLAGS) -c  $< -o $@
$(NAME): $(OBJ) libft.h
	ar rcs $(NAME) $(OBJ)
clean:
	rm -f $(OBJ) *.d
fclean: clean
	rm -f $(NAME)
re: fclean all

