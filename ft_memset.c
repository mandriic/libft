#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*dest;

	dest = b;
	while (len > 0)
	{
		dest[len - 1] = c;
		len--;
	}
	return (dest);
}
