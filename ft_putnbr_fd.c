#include "libft.h"

void ft_putnbr_fd(int n, int fd)
{
	char	*temp;
	int		i;

	i = 0;
	temp = ft_itoa(n);
	while(temp[i] != '\0')
		write(fd, &temp[i++], 1);
	free(temp);
}
