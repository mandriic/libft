#include "libft.h"

#define teststring "123456789" //dst
#define teststring2 "9876540" //src
#define testnum 5
int main ()
{
	int i;
	i = '-';
	printf("Symbol is\n");
	write (1, &i, 1);	
	printf("\n");
	if (isalpha(i) && ft_isalpha(i))
		printf("isalpha(true) alphabetic character \n");
	if (!isalpha(i) && !ft_isalpha(i))
		printf("isalpha(false) alphabetic character \n");
	if (isdigit(i) && ft_isdigit(i))
		printf("isdigit(true) checks for a digit \n");
	if (!isdigit(i) && !ft_isdigit(i))
		printf("isdigit(false) checks for a digit \n");
	if (isalnum(i) && ft_isalnum(i))
		printf("isalnum(true) letters + number \n");
	if (!isalnum(i) && !ft_isalnum(i))
		printf("isalnum(false) letters + number \n");
	if (isascii(i) && ft_isascii(i))
		printf("isascii(true) ascii \n");
	if (!isascii(i) && !ft_isascii(i))
		printf("isascii(false) ascii \n");
	if (isprint(i) && ft_isprint(i))
		printf("isprint(true) Caracteres ASCII imprimibles\n\n");
	if (!isprint(i) && !ft_isprint(i))
		printf("isprint(false) Caracteres ASCII imprimibles \n\n");
	
	//strlen
	char string[] = teststring;
	printf("Word of string is - %s\n", teststring);
	printf("strlen - %lu ft_strlen - %lu\n", strlen(string), ft_strlen(string));
	
	//memset
	char arr_memset[] = "teststring";
	char arr_ft_memset[] = "teststring";
	memset(arr_memset, '5', testnum * sizeof(char));
	ft_memset(arr_ft_memset, '5', testnum * sizeof(char));
	printf("%s\n", arr_memset);
	printf("%s\n", arr_ft_memset);

	//bzero
	bzero(arr_memset, testnum * sizeof(char));
	ft_bzero(arr_ft_memset, testnum * sizeof(char));
	printf("%s<- after bzero\n", arr_memset);
	printf("%s<- after ft_bzero\n", arr_ft_memset);

	//memcpy
	char memcpysrc[50]= teststring;
	//char memcpydest[50] = "12345";

	memcpy(memcpysrc + 4, memcpysrc, 10);
	printf("Src for memcpy\t\t%s\n", memcpysrc);
	//printf("Dest of memcpy\t\t%s\n", memcpydest);

	char memcpysrc2[50]= teststring;
	//char memcpydest2[50] = "12345";

	ft_memcpy(memcpysrc2 + 4, memcpysrc2, 10);
	printf("Src for ft_memcpy\t%s\n", memcpysrc2);
	//printf("Dest of ft_memcpy\t%s\n", memcpydest2);

	char memcpysrc3[50]= teststring;
	memmove(memcpysrc3 + 2, memcpysrc3, 10);
	printf("Src for memmove\t\t%s\n", memcpysrc3);

	char memcpysrc4[50]= teststring;
	ft_memmove(memcpysrc4 + 2, memcpysrc4, 10);
	printf("Src for ft_memmove\t%s\n", memcpysrc4);

	//toupper tolower
	printf("%c toupper\n", toupper(i));
	printf("%c ft_toupper\n", ft_toupper(i));
	printf("%c tolower\n", tolower(i));
	printf("%c ft_tolower\n", ft_tolower(i));

	// //strlcpy
	// char arr_dst_strlcpy[] = teststring;
	// char arr_src_strlcpy[] = teststring2;
	// printf("%lu strlcpy dest\n", strlcpy(arr_dst_strlcpy, arr_src_strlcpy, sizeof(char) * testnum));
	// printf("%s\n", arr_dst_strlcpy);

	// char arr_dst_ft_strlcpy[] = teststring;
	// char arr_src_ft_strlcpy[] = teststring2;
	// printf("%lu ft_strlcpy dest\n", ft_strlcpy(arr_dst_ft_strlcpy, arr_src_ft_strlcpy, sizeof(char) * testnum));
	// printf("%s\n", arr_dst_ft_strlcpy);

	char arr_dst_strlcat[15];
	arr_dst_strlcat[0] = 'a';
	arr_dst_strlcat[1] = 'b';
	arr_dst_strlcat[2] = 'c';

	// char arr_src_strlcat[] = teststring2;
	// printf("%lu strlcat ________________________dest\n", strlcat(arr_dst_strlcat, arr_src_strlcat, 11 ));
	printf("%s\n", arr_dst_strlcat);
	// strlcat(arr_dst_strlcat, arr_src_strlcat, sizeof(char) * testnum);
	// printf("%s\n", arr_dst_strlcat);


	// //calloc
	// char *puntero;
	// puntero = calloc(4, sizeof(char));
	// printf("%s\n", puntero);

	// int str_memch[7] = {-49, 49, 1, -1, 0, -2, 2};
	// printf ("%p\n", (char*)memchr(str_memch, 'a' ,7));
	// printf ("%p\n", (char*)ft_memchr(str_memch, 'a' ,7));

	char str1[50] = "12asd,3,45,fgff,6,ff,f,f,fff,f,ff,,,,78asd9,asd,";
	char str2[15] = ",";
	// int ret;

	
	// printf("memcmp%d\n", memcmp(str1, str2, 5));
	// printf("ft_memcmp%d\n", ft_memcmp(str1, str2, 5));

	// char *strdup_test = strdup(str1);
	// printf("%s\n", strdup_test);
	// strdup_test = ft_strdup(str1);
	// printf("%s\n", strdup_test);
	// printf("%s\n", str1);

	// printf("%ld ft_strlcat\n", ft_strlcat(str1, str2, 5));
	// printf("%ld strlen str1 \n", ft_strlen(str1));
	// printf("%s\n", str1);
	// printf("%d\n", ft_strncmp(str1, str2, 3));
	// strnstr(str1, str2, 2);


	// 	printf("%d ftatoi\n", ft_atoi("+-54"));
	// 	printf("%d atoi\n", atoi("+-54"));
	// 	printf("%d\n",ft_atoi("-+48"));
	// 	printf("%d\n",atoi("-+48"));
	// 	printf("%d\n",ft_atoi("--47"));
	// 	printf("%d\n",atoi("--47"));
	// 	printf("%d\n",ft_atoi("++47"));
	// 	printf("%d\n",atoi("++47"));
	// 	printf("%d\n",ft_atoi("+47+5"));
	// 	printf("%d\n",atoi("+47+5"));
	// 	printf("\n");
	// 	printf("%zu\n", ft_strlen(str1));
	// 	printf("%s str1 \n", str1);
	// 	char *arrsubstr = ft_substr(str1, 3, 3);
	// 	printf("%s\n", arrsubstr);
		// printf("\n");
		// printf("\n");
		// printf("str1-%s str2-%s\n", str1, str2);
		// char *arrstrjoin = ft_strjoin(str1, str2);
		// printf("arrjoin - %s\n", arrstrjoin);

		// char *arrstrtrim;
		// arrstrtrim = ft_strtrim(str1, str2);
		// printf("%s\n", arrstrtrim);
		printf("str1-%s str2-%s\n", str1, str2);
		ft_split(str1, ',');
		char **tabstr; //= ft_split("lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultricies diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.", 'i');
	//	printf("ft_split %s\n", tabstr[0]);
		if (!(tabstr = ft_split("", 'z')))
                        printf("NULL");
                else
                        if (!tabstr[0])
                                printf("ok\n");                  
	return(0);
}