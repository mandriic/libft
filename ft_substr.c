/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mandriic <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 16:33:58 by mandriic          #+#    #+#             */
/*   Updated: 2021/10/01 16:34:03 by mandriic         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*p;
	size_t	count;

	p = (char *)malloc(sizeof(char) * len + 1);
	if (NULL == p)
		return (0);
	if (ft_strlen(s) < start + len)
		return ("");
	count = 0;
	while (s[start] != '\0' && len != 0)
	{
		p[count] = s[start];
		start++;
		count++;
		len--;
	}
	p[count] = '\0';
	return (p);
}
