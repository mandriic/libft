#include "libft.h"

char	**ft_split(char const *s, char c)
{
	char	**str;
	int		wds;
	int		i;
	int		i2;
	int 	start;
	int 	end;

	wds = 0;
	i = 0;
	// if (s[0] == '\0')
	// 	return (0);
	while (s[i] == c)
		i++;
	while (s[i] != '\0')
	{
		if (ft_strchr(&c, s[i]) && s[i + 1] != c && s[i + 1] != '\0' )
			wds++;
		i++;
	}
//	printf("%d\n", wds);
	str = (char**) malloc(sizeof(char*) * (wds + 1));
	if (NULL == str)
		return(0);
	i = 0;
	i2 = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
		{
			i++;
			continue;
		}
		start = i;
		while (s[i] != c && s[i] != '\0')
			i++;
		end = i;
		str[i2] = ft_substr(s, start, end - start);
		i2++;
	}
//	str[i2] = NULL;
	return(str);
}
