/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mandriic <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 14:03:14 by mandriic          #+#    #+#             */
/*   Updated: 2021/10/01 14:03:21 by mandriic         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	is;
	size_t	in;
	size_t	lenmem;

	lenmem = len;
	is = 0;
	in = 0;
	if (*needle == 0)
		return ((char *)haystack);
	while (haystack[is] != needle[in] && haystack[is] != '\0' && len != 0)
	{
		is++;
		len--;
	}
	lenmem = is;
	while (haystack[is] == needle[in] && len != 0)
	{
		is++;
		in++;
		len--;
	}
	if (needle[in] == '\0')
		return ((char *)haystack + lenmem);
	else
		return (0);
}
