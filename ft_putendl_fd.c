#include "libft.h"

void ft_putendl_fd(char *s, int fd)
{
	int i;

	write(fd, "\n", 1);
	i = 0;
	while(s[i++] != '\0')
		write(fd, &s[i], 1);
}
